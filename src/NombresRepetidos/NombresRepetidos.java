package NombresRepetidos;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;




public class NombresRepetidos  {


	private int cantidadChicos;
	private int cantidadNombresFrecuentes;
	Map<String, Integer> nombres;
	private int j;
	private int i;
	

	public NombresRepetidos(String direccion) throws NumberFormatException, IOException {
		super();
		this.setCantidadChicos(cantidadChicos);
		this.setCantidadNombresFrecuentes(cantidadNombresFrecuentes);
		nombres=  new TreeMap<String, Integer>();
		leerArchivo(direccion);
		mostrarRepetidos();
		
	
	}

//Metodo de lectura del archivo
	public  void leerArchivo(String direccion) throws NumberFormatException, IOException {
		
		File f = new File(direccion);
		
		Scanner entrada = new Scanner(f);
		    
	 	    
	 	    
	 	    //leemos la primer entrada del archivo que corresponde a la cantidad de aplicaciones instaladas
		this.setCantidadChicos(entrada.nextInt());
	 	  
		    //leemos la segunda entrada entrada del archivo que corresponde a la cantidad de Memoria que necesitamos.
		this.setCantidadNombresFrecuentes(entrada.nextInt()); 
	 	    
	 	  //cargamos el treeMap con los nombres
	 	    
		for(int i=0;i<cantidadChicos;i++){
			String k = entrada.next();
			int cantidad=0;
			if(nombres.containsKey(k)){
				cantidad = nombres.get(k);
				nombres.put(k, cantidad+1);	
			}				
			else
				nombres.put(k, 1);	
			
		}
			
	 	  
	 	  //cerrar Archivo
	 	  
			entrada.close();
		
			
		}
	
	//Metodo de busqueda de Nombres repetidos
	
	public void mostrarRepetidos() throws IOException {
		
		
		SortedMap <Integer, String>repetidos=  new TreeMap<Integer, String>(java.util.Collections.reverseOrder());
		
		
		for (Entry<String, Integer> nombre : nombres.entrySet())
			repetidos.put(nombre.getValue(), nombre.getKey());
		
	for(Entry<Integer, String> tmp: repetidos.entrySet()) {
		
	System.out.println(tmp.getKey());
	System.out.println(tmp.getValue());
	}
	
	crearArchivosSalida(repetidos);
	}
	
	
	//crear archivo de salida
	public  void crearArchivosSalida(Map <Integer, String>repetidos) throws IOException {

		PrintWriter s = new PrintWriter(new FileWriter("Archivo/nombres.out")); 
		
		for(Entry<Integer, String> tmp: repetidos.entrySet()) {
			
			s.print(tmp.getValue()+"  "+ tmp.getKey());
			
			s.println();
			}
		
		s.close();
}
		
		
		

	


	public int getCantidadChicos() {
		return cantidadChicos;
	}


	public void setCantidadChicos(int cantidadChicos) {
		this.cantidadChicos = cantidadChicos;
	}


	public int getCantidadNombresFrecuentes() {
		return cantidadNombresFrecuentes;
	}


	public void setCantidadNombresFrecuentes(int cantidadNombresFrecuentes) {
		this.cantidadNombresFrecuentes = cantidadNombresFrecuentes;
	}


	public static void main(String[] args) throws NumberFormatException, IOException {
		NombresRepetidos NR=new NombresRepetidos("Archivos/nombres.in");
		
		
	}
	
	}



